package com.clickhouse.sample.jdbc.spring;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试父类，其他测试类可直接继承此类。帮助配置spring环境
 *
 * @author chenjiacheng
 * @CREATE 2019/7/11
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class BaseTest {

}
