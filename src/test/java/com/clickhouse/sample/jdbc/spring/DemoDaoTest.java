package com.clickhouse.sample.jdbc.spring;


import com.clickhouse.sample.jdbc.dao.DemoDao;
import com.clickhouse.sample.jdbc.entity.DemoEntity;
import com.clickhouse.sample.jdbc.utils.MyDataUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.yandex.clickhouse.ClickHouseDataSource;

import java.util.List;

/**
 * @author chenjiacheng
 * @E-mail 1320743201@qq.com
 * @CREATE 2019-07-02
 */
public class DemoDaoTest extends BaseTest {

    @Autowired
    private DemoDao demoDao;

    @Test
    public void testInsert(){

        List<DemoEntity> list = MyDataUtil.genDemoList( 100, 0 );

        long time = System.currentTimeMillis();
        //demoDao.batchInsert(  list );
        long end = System.currentTimeMillis();

        System.out.println( "use time : " + (end - time) );
    }

    @Test
    public void testSelect(){

        List<DemoEntity> data = demoDao.selectList( "label_name asc" );
        print(data);
    }


    private void print( List<DemoEntity> lst ){

        for( DemoEntity d : lst ){
            System.out.println( d.getLabelId() + " : " + d.getLabelName() + " : " + d.getDate().toString() );
        }
    }



}
