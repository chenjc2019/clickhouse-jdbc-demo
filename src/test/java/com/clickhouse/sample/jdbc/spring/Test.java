package com.clickhouse.sample.jdbc.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

/**
 * @author chenjiacheng
 * @E-mail 1320743201@qq.com
 * @CREATE 2019-07-02
 */
public class Test {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        ClickHouseProperties clickHouseProperties = context.getBean( ClickHouseProperties.class );
        System.out.println( clickHouseProperties.getUser() );
        System.out.println( clickHouseProperties.getDatabase() );
        System.out.println( clickHouseProperties.getPassword() );

        ClickHouseDataSource clickHouseDataSource = context.getBean( ClickHouseDataSource.class );

        System.out.println( clickHouseDataSource.getHost() );
    }


}
