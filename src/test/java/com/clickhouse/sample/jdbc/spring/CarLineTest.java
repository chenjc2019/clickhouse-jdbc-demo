package com.clickhouse.sample.jdbc.spring;

import com.clickhouse.sample.jdbc.dao.CarLineDao;
import com.clickhouse.sample.jdbc.entity.TabCarLine;
import com.clickhouse.sample.jdbc.utils.MyCarLineUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class CarLineTest extends  BaseTest {

    @Autowired
    private CarLineDao carLineDao;


    @Test
    public void testInsert(){

        int count = 1;
        for( int cycle = 0; cycle < count; cycle ++ ){
            int size = 1 ; // 100 wan, 10.471, 300 wan 62.759
            System.out.println( " begin gen data " );
            List<TabCarLine> list = new ArrayList<TabCarLine>(  );
            for( int i = 0; i < (size  ) ; i++ ){
                list.add(MyCarLineUtils.getData());
            }

            System.out.println( " end gen data " );
            System.out.println( " begin insert " );
            long begin = System.currentTimeMillis();
            carLineDao.batchInsert( list );
            list.clear();
            list = null;

            System.out.println( " end insert " );
            long end = System.currentTimeMillis();

            System.out.println(  end - begin );
        }
    }

    @Test
    public void selectAllPage(){
        System.out.println( "selectAllPage() :" );
        String sql = "select PassTime,TollgateID,   PlateNo,   PlateColor,  StorageUrl1, StorageUrl2 , VehicleClass  from t_carline_new_all  order by PassTime desc limit 1,10";
        carLineDao.executeQuery( sql );
    }

    @Test
    public void selectByPassTime(){
        System.out.println( "selectByPassTime():" );
        String sql = "select PassTime,TollgateID,   PlateNo,   PlateColor,  StorageUrl1, StorageUrl2 , VehicleClass  from t_carline_new_all   where PassTime >= toDateTime('2019-07-20 00:00:02')  order by PassTime desc limit 10,20";
        carLineDao.executeQuery( sql );
    }

    @Test
    public void selectByTollgateID(){
        System.out.println( "selectByTollgateID():" );
        // String sql = "select TollgateID,  PassTime,   PlateNo,   PlateColor,  StorageUrl1, StorageUrl2 , VehicleClass from t_carline_new_all where TollgateID in ('44060785663151898572961','44060607968257742819695') order by PassTime desc limit 15";
        String sql = "select PassTime,TollgateID,  PlateNo,   PlateColor,  StorageUrl1, StorageUrl2 , VehicleClass from t_carline_new_all where TollgateID in ('44060607968257742819695','44060785663151898572961') order by PassTime desc  limit 1,20";
         carLineDao.executeQuery( sql );
         // 0.325s, 0.301s ,
    }

    @Test
    public void selectByPlateNo(){
        System.out.println( "selectByPlateNo() : " );
        String sql = "select PassTime,TollgateID, PlateNo,   PlateColor,  StorageUrl1, StorageUrl2 , VehicleClass  from t_carline_new_all   where  PlateNo like '粤%' order by PassTime desc  limit 10,20";
        carLineDao.executeQuery( sql );
        // 1.370s,
    }

    long begin ,end;
    @Before
    public void before(){
        begin = System.currentTimeMillis();

    }

    @After
    public void after(){
        end = System.currentTimeMillis();
        long use = end - begin;
        System.out.println( " use time : " + use );

    }


}
