package com.clickhouse.sample.jdbc.utils;

import com.clickhouse.sample.jdbc.entity.TabCarLine;

import java.util.*;

public class MyCarLineUtils {


    private static char[] A_z = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0','1','2','3','4','5','6','7','8','9'
            //'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };

    private static char[] Num = {  '0','1','2','3','4','5','6','7','8','9' };

    private static String[] gateArray = {
            "44060464157004763716927",
            "44060768441529627630826",
            "44060507204300641985349",
            "44060196429300920356477",
            "44060906590354371544273",
            "44060523357755106765643",
            "44060739677869974448677",
            "44060123615828035677711",
            "44060207722834889592769",
            "44060198113286046674866",
            "44060257687940655534490",
            "44060947505247440389841",
            "44060001561725667202724",
            "44060333453276783123557",
            "44060286549827330934764",
            "44060614316254116623284",
            "44060616957178439700726",
            "44060557466148604622212",
            "44060243010107004750448",
            "44060616992550558740326",
            "44060065453258911041003",
            "44060614316254116623284",
            "44060523357755106765643",
            "44060575262490829495141",
            "44060498372989085290549",

    };


    public static void main(String[] args) {
        System.out.println( getPlateNo("粤")  + "--" + getTollgate(22));
    }

    public static String getPlateNo( String prefix ){
        return prefix + getAToZ(6);
    }


    public static String getAToZ( int count ){
        if( count <= 0 ){
            count = 6;
        }
        Random r = new Random();
        String str = "";
        for( int index = 1;index <= count; index++ ){
            int sub = r.nextInt(A_z.length);
            str += A_z[sub];
        }
        return str;

    }

    public static String getTollgate( int count ){
        Random r = new Random();
        String str = "";
        for( int index = 1;index <= count; index++ ){
            int sub = r.nextInt( Num.length );
            str += Num[sub];
        }
        return "44060" + str;
    }

    public static String getTollgate( ){
        Random r = new Random();
        String str = "";
        int sub = r.nextInt( gateArray.length );
        return gateArray[sub];
    }

    public static TabCarLine getData(){
        String carNo = getPlateNo("粤");
        TabCarLine one = new TabCarLine();
        one.setPassTime( new Date() );
        one.setPassNo( getTollgate(22)  );
        one.setPlateNo( carNo ) ;
        one.setCreateTime( new Date() );
        one.setTollgateID( getTollgate() );
        return one;
    }


}
