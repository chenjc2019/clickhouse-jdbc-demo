package com.clickhouse.sample.jdbc.utils;

import java.util.Date;

/**
 * @author chenjiacheng
 * @CREATE 2019/7/11
 */
public class MyFirstDemo {

    public static void main(String[] args) {
        System.out.println( new Date().toString());
    }

}
