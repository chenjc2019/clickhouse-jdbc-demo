package com.clickhouse.sample.jdbc.utils;

import com.clickhouse.sample.jdbc.entity.BaseEntity;
import com.clickhouse.sample.jdbc.entity.DemoEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 模拟数据产生工具类
 * @author chenjiacheng
 * @CREATE 2019-06-17 16:00
 */
public class MyDataUtil {

    private final static String test = "If,you,see,this,page,the,nginx,web,server,is,successfully,installed,and,working.,Further,configuration,is,required,For,online,documentation,and,support,please,refer,to,Commercial,support,is,available,at,Thank,you,for,using,nginx";

    public static void main(String[] args) {

        String val = "10";
        System.out.println( val.hashCode()  );

        System.out.println( val.hashCode() % 36 );
    }

    /**
     * 根据需要产生数据
     * @param dataSize      模拟需要产生的数据量
     * @return
     */
    public static List<Object[]> genData( int dataSize, int beginId ){

        List<Object[]> data = new ArrayList<Object[]>( dataSize );
        String[] label = test.split(",");

        for( int i = 1; i <= dataSize; ++i ){
            Object[] obj = new Object[2];
            int id = i + beginId;
            String idStr =  id +"";
            obj[0] =  id ;
            obj[1] = label[ idStr.hashCode() % label.length ];

            data.add( obj );
        }
        return data;
    }

    /**
     * 创建数据方法
     * @param dataSize
     * @param beginId
     * @return
     */
    public static List<DemoEntity> genDemoList( int dataSize, int beginId ){
        long begin = System.currentTimeMillis();

        List<DemoEntity> data = new ArrayList<DemoEntity>( dataSize );
        String[] label = test.split(",");

        for( int i = 1; i <= dataSize; ++i ){
            long id = i + beginId;
            String idStr =  id +"";
            DemoEntity demoEntity = new DemoEntity();
            demoEntity.setLabelId( id );
            demoEntity.setLabelName( label[ idStr.hashCode() % label.length ] );

            data.add( demoEntity );
        }

        long end = System.currentTimeMillis();
        long count = end - begin;
        System.out.println( " create data use time : " +  count + " ms" );
        return data;
    }

    public static List<BaseEntity> genBaeList( int dataSize, int beginId ){
        long begin = System.currentTimeMillis();

        List<BaseEntity> data = new ArrayList<BaseEntity>( dataSize );
        String[] label = test.split(",");

        for( int i = 1; i <= dataSize; ++i ){
            int id = i + beginId;
            String idStr =  id +"";
            BaseEntity demoEntity = new BaseEntity();
            demoEntity.setId( id );
            demoEntity.setName( label[ idStr.hashCode() % label.length ] );

            data.add( demoEntity );
        }

        long end = System.currentTimeMillis();
        long count = end - begin;
        System.out.println( " create data use time : " +  count + " ms" );
        return data;
    }

}
