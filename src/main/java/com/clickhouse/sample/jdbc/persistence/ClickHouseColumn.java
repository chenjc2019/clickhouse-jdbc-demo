package com.clickhouse.sample.jdbc.persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于描述ClickHouse表字段
 *
 * @author chenjiacheng
 */

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClickHouseColumn {

    /**
     * 对应表字段
     */
    String name() default "";

    /**
     * 表字段的格式
     * 比如日期类型：yyyy-MM-dd HH:mm:ss
     */
    String format() default "";

}
