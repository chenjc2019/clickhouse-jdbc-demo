package com.clickhouse.sample.jdbc.persistence;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClickHouse 表名注解
 * 用于描述对应的表名称
 *
 * @author chenjiacheng
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClickHouseTable {

    /**
     * 对应表名
     */
    String name() default "";
}
