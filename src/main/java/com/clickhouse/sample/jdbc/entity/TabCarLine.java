package com.clickhouse.sample.jdbc.entity;


import com.clickhouse.sample.jdbc.persistence.ClickHouseColumn;
import com.clickhouse.sample.jdbc.persistence.ClickHouseTable;

import java.util.Date;

//@ClickHouseTable( name = "tab_carline_all")
@ClickHouseTable( name = "t_carline_new_all")
public class TabCarLine {

    @ClickHouseColumn( name = "LaneNo" )
    private String LaneNo = "1";

    @ClickHouseColumn( name = "PassNo" )
    private String PassNo;

    @ClickHouseColumn( name = "MarkTime", format = "yyyy-MM-dd HH:mm:ss" )
    private Date MarkTime = new Date() ;

    @ClickHouseColumn( name = "AppearTime" , format = "yyyy-MM-dd HH:mm:ss")
    private Date AppearTime = new Date();

    @ClickHouseColumn( name = "DisappearTime" , format = "yyyy-MM-dd HH:mm:ss")
    private Date DisappearTime = new Date();

    @ClickHouseColumn( name = "NameOfPassedRoad" )
    private String NameOfPassedRoad;

    @ClickHouseColumn( name = "HasPlate" )
    private String HasPlate = "1";

    @ClickHouseColumn( name = "IDENTIFY" )
    private String IDENTIFY = "一点停";

    @ClickHouseColumn( name = "ENTERCHANNELNAME" )
    private String ENTERCHANNELNAME ="96岗进口";

    @ClickHouseColumn( name = "PARKCODE" )
    private String PARKCODE = "2KM9HX9W";

    @ClickHouseColumn( name = "LOCKKEY" )
    private String LOCKKEY  = "1b675e1e018715d09a972e2e3edc69e64416390c";

    @ClickHouseColumn( name = "UploadTime" , format = "yyyy-MM-dd HH:mm:ss")
    private Date UploadTime  =new Date();

    @ClickHouseColumn( name = "Speed" )
    private String Speed = "0.0";

    @ClickHouseColumn( name = "LeftTopX" )
    private Long LeftTopX = 0l;

    @ClickHouseColumn( name = "LeftTopY" )
    private Long LeftTopY = 0l;
    @ClickHouseColumn( name = "RightBtmX" )
    private Long RightBtmX = 0l;
    @ClickHouseColumn( name = "RightBtmY" )
    private Long RightBtmY = 0l;
    @ClickHouseColumn( name = "VehicleLength" )
    private Long VehicleLength = 0l;

    @ClickHouseColumn( name = "VehicleWidth" )
    private Long VehicleWidth = 0l;

    @ClickHouseColumn( name = "VehicleHeight" )
    private Long VehicleHeight = 0l;

    @ClickHouseColumn( name = "NumOfPassenger" )
    private Long NumOfPassenger = 0l;

    @ClickHouseColumn( name = "PlateNo" )
    private String PlateNo;

    @ClickHouseColumn( name = "TollgateID" )
    private String TollgateID;

    @ClickHouseColumn( name = "PassTime", format = "yyyy-MM-dd HH:mm:ss")
    private Date PassTime;

    @ClickHouseColumn( name = "PlateClass" )
    private String PlateClass = "0.0'";

    @ClickHouseColumn( name = "PlateColor" )
    private String PlateColor = "0.0";

    @ClickHouseColumn( name = "StorageUrl1" )
    private String StorageUrl1 = "\"https://open.yidianting.xin/openydt/files/images/161231045026507645954998?s=2a6d3e3e\"";

    @ClickHouseColumn( name = "StorageUrl2" )
    private String StorageUrl2 =  ":\"https://open.yidianting.xin/openydt/files/images/161231045026507645954998?s=755790b5\"";

    @ClickHouseColumn( name = "VehicleClass" )
    private String VehicleClass;

    @ClickHouseColumn( name = "CREATETIME", format = "yyyy-MM-dd HH:mm:ss")
    private Date CreateTime =new Date();


    public String getLaneNo() {
        return LaneNo;
    }

    public void setLaneNo(String laneNo) {
        LaneNo = laneNo;
    }

    public String getPassNo() {
        return PassNo;
    }

    public void setPassNo(String passNo) {
        PassNo = passNo;
    }

    public Date getMarkTime() {
        return MarkTime;
    }

    public void setMarkTime(Date markTime) {
        MarkTime = markTime;
    }

    public Date getAppearTime() {
        return AppearTime;
    }

    public void setAppearTime(Date appearTime) {
        AppearTime = appearTime;
    }

    public Date getDisappearTime() {
        return DisappearTime;
    }

    public void setDisappearTime(Date disappearTime) {
        DisappearTime = disappearTime;
    }

    public String getNameOfPassedRoad() {
        return NameOfPassedRoad;
    }

    public void setNameOfPassedRoad(String nameOfPassedRoad) {
        NameOfPassedRoad = nameOfPassedRoad;
    }

    public String getHasPlate() {
        return HasPlate;
    }

    public void setHasPlate(String hasPlate) {
        HasPlate = hasPlate;
    }

    public String getIDENTIFY() {
        return IDENTIFY;
    }

    public void setIDENTIFY(String IDENTIFY) {
        this.IDENTIFY = IDENTIFY;
    }

    public String getENTERCHANNELNAME() {
        return ENTERCHANNELNAME;
    }

    public void setENTERCHANNELNAME(String ENTERCHANNELNAME) {
        this.ENTERCHANNELNAME = ENTERCHANNELNAME;
    }

    public String getPARKCODE() {
        return PARKCODE;
    }

    public void setPARKCODE(String PARKCODE) {
        this.PARKCODE = PARKCODE;
    }

    public String getLOCKKEY() {
        return LOCKKEY;
    }

    public void setLOCKKEY(String LOCKKEY) {
        this.LOCKKEY = LOCKKEY;
    }

    public Date getUploadTime() {
        return UploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        UploadTime = uploadTime;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public Long getLeftTopX() {
        return LeftTopX;
    }

    public void setLeftTopX(Long leftTopX) {
        LeftTopX = leftTopX;
    }

    public Long getLeftTopY() {
        return LeftTopY;
    }

    public void setLeftTopY(Long leftTopY) {
        LeftTopY = leftTopY;
    }

    public Long getRightBtmX() {
        return RightBtmX;
    }

    public void setRightBtmX(Long rightBtmX) {
        RightBtmX = rightBtmX;
    }

    public Long getRightBtmY() {
        return RightBtmY;
    }

    public void setRightBtmY(Long rightBtmY) {
        RightBtmY = rightBtmY;
    }

    public Long getVehicleLength() {
        return VehicleLength;
    }

    public void setVehicleLength(Long vehicleLength) {
        VehicleLength = vehicleLength;
    }

    public Long getVehicleWidth() {
        return VehicleWidth;
    }

    public void setVehicleWidth(Long vehicleWidth) {
        VehicleWidth = vehicleWidth;
    }

    public Long getVehicleHeight() {
        return VehicleHeight;
    }

    public void setVehicleHeight(Long vehicleHeight) {
        VehicleHeight = vehicleHeight;
    }

    public Long getNumOfPassenger() {
        return NumOfPassenger;
    }

    public void setNumOfPassenger(Long numOfPassenger) {
        NumOfPassenger = numOfPassenger;
    }

    public String getPlateNo() {
        return PlateNo;
    }

    public void setPlateNo(String plateNo) {
        PlateNo = plateNo;
    }

    public String getTollgateID() {
        return TollgateID;
    }

    public void setTollgateID(String tollgateID) {
        TollgateID = tollgateID;
    }

    public Date getPassTime() {
        return PassTime;
    }

    public void setPassTime(Date passTime) {
        PassTime = passTime;
    }

    public String getPlateClass() {
        return PlateClass;
    }

    public void setPlateClass(String plateClass) {
        PlateClass = plateClass;
    }

    public String getPlateColor() {
        return PlateColor;
    }

    public void setPlateColor(String plateColor) {
        PlateColor = plateColor;
    }

    public String getStorageUrl1() {
        return StorageUrl1;
    }

    public void setStorageUrl1(String storageUrl1) {
        StorageUrl1 = storageUrl1;
    }

    public String getStorageUrl2() {
        return StorageUrl2;
    }

    public void setStorageUrl2(String storageUrl2) {
        StorageUrl2 = storageUrl2;
    }

    public String getVehicleClass() {
        return VehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        VehicleClass = vehicleClass;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }
}
