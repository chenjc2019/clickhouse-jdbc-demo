package com.clickhouse.sample.jdbc.entity;

import com.clickhouse.sample.jdbc.persistence.ClickHouseColumn;
import com.clickhouse.sample.jdbc.persistence.ClickHouseTable;

import java.util.Date;


/**
 * @author chenjiacheng
 * @CREATE 2019-06-28 11:11
 */
@ClickHouseTable( name = "t_demo_all" )
public class DemoEntity {

    @ClickHouseColumn( name = "label_id" )
    private Long labelId;

    @ClickHouseColumn( name = "label_name" )
    private String labelName;

    @ClickHouseColumn(name = "date", format = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    public Long getLabelId() {
        return labelId;
    }

    public void setLabelId(Long labelId) {
        this.labelId = labelId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
