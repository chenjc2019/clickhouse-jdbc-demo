package com.clickhouse.sample.jdbc.entity;

import com.clickhouse.sample.jdbc.persistence.ClickHouseColumn;
import com.clickhouse.sample.jdbc.persistence.ClickHouseTable;

import java.util.Date;

/**
 * @author chenjiacheng
 * @CREATE 2019/7/3
 */

@ClickHouseTable( name = "t_other")
public class OtherEntity {

    @ClickHouseColumn(name = "id")
    private Long id;

    @ClickHouseColumn(name = "date", format = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @ClickHouseColumn( name = "str" )
    private String str;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}


