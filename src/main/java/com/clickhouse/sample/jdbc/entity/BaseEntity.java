package com.clickhouse.sample.jdbc.entity;

import com.clickhouse.sample.jdbc.persistence.ClickHouseColumn;
import com.clickhouse.sample.jdbc.persistence.ClickHouseTable;


/**
 * @author chenjiacheng
 * @CREATE 2019/6/28
 */

@ClickHouseTable( name = "t_base_all" )
public class BaseEntity {

    @ClickHouseColumn( name = "id")
    private Integer id;

    @ClickHouseColumn( name = "name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
