package com.clickhouse.sample.jdbc.dao;

import com.clickhouse.sample.jdbc.entity.OtherEntity;
import org.springframework.stereotype.Repository;

/**
 * @author chenjiacheng
 * @CREATE 2019/7/3
 */
@Repository
public class OtherDao extends AbstractClickHouseDao<OtherEntity> {


}
