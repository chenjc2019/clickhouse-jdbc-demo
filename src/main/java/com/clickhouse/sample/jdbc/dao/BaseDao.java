package com.clickhouse.sample.jdbc.dao;

import com.clickhouse.sample.jdbc.entity.BaseEntity;
import org.springframework.stereotype.Repository;

/**
 * @author chenjiacheng
 * @CREATE 2019/6/28
 */
@Repository
public class BaseDao extends AbstractClickHouseDao<BaseEntity> {
}
