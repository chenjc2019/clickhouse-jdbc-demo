package com.clickhouse.sample.jdbc.dao;

import com.clickhouse.sample.jdbc.entity.DemoEntity;
import org.springframework.stereotype.Repository;

/**
 * @author chenjiacheng
 * @CREATE 20190628 11:11
 */
@Repository
public class DemoDao extends AbstractClickHouseDao<DemoEntity> {

}
