package com.clickhouse.sample.jdbc.dao;

import com.clickhouse.sample.jdbc.entity.TabCarLine;
import org.springframework.stereotype.Repository;

@Repository
public class CarLineDao extends AbstractClickHouseDao<TabCarLine> {
}
